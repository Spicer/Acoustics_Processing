#!/usr/bin/env python

import numpy as np
import glob
import matplotlib.pyplot as plt
import matplotlib
from scipy.io import loadmat
from scipy.fftpack import rfft, fft, ifft

# from melbank import compute_melmat


class voice():
    def __init__(self):

        self._data_dict = {}
        self.index = 0
        self.time = []

    def load(self):
        """ bring in all 20 voise files """

        # create subplots for phase, mag, and coherence
        f, self.ax = plt.subplots(3, 1)
        f2, self.bx = plt.subplots(2, 1, sharex=True)

        # find all the mat files in local directory
        file_mats = glob.glob("jack_data.mat")


        # load each file
        for mat in file_mats:

            f = loadmat(mat)
            self._data_dict[str(self.index)] = f

            # for key in self._data_dict.iterkeys:
            #     print( key)
            for key in self._data_dict[str(self.index)].keys():
                print("key")
                print( key)
            for value in self._data_dict[str(self.index)].values():
                print( value)

            print( "data")
            data = self._data_dict[str(self.index)]["data"]
            print( "samples: ", data[0].shape)
            print( "toia;l shape: ", data.shape)
            self.time = np.linspace(0, data.shape[0], data.shape[0])

            print( self.time.shape)

        for ii in range(data[0].shape[0]):
            print( np.mean(data[:, ii]))

            curr_data = np.array((data[:, ii]), dtype=float)
            print( curr_data.shape)
            self.ax[0].plot(self.time, curr_data)
        self.ax[1].plot(data[:, 0])
        plt.show()


if __name__ == "__main__":

    exe = voice()

    exe.load()
