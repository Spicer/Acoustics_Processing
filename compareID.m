function [p_val_freq, p_val_mag] = compareID(test_id, reference_struct)

dist = cdf(reference_struct.freq_pd, test_id(1,1));
p_val_freq = min([dist, 1 - dist]);

kl_div = KLDiv(squeeze(test_id(:,2)).', squeeze(mean(reference_struct.cont(:, 2, :), 3)).');
dist =  cdf(reference_struct.kl_pd, kl_div);
p_val_mag = min([dist, 1 - dist]);

end