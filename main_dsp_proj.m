close all
clear

addpath(genpath('C:\Users\mwspi\Documents\VT\Research\Wicks\Matlab\'))

num_cep = 12;

% tic
load('trained_data.mat')
load('unknown_data1.mat')
sample_rate = 16e3;
unknown_cep = zeros(num_cep, size(data, 2));
for col = 1:size(data, 2)
    unknown_cep(:, col) = analyze(data(:, col), sample_rate, num_cep);
end
figure(); plot(unknown_cep); hold on; plot(jack_cep_av)
figure(); plot(jack_cep)

unknown_csim = zeros(size(data, 2), 1);
unknown_norm = zeros(size(data, 2), 1);
unknown_dnorm = zeros(size(data, 2), 1);
for col = 1:size(data, 2)
    unknown_csim(col) = cosineSimilarity(unknown_cep(:, col), jack_cep_av);
    unknown_norm(col) = norm(unknown_cep(:, col) - jack_cep_av);
    unknown_dnorm(col) = norm(diff(unknown_cep(:, col)) - diff(jack_cep_av), 2);
%     disp(min(corrcoef(jack_cep(:, col), jack_cep_av)))
%     disp(min(corrcoef(polyfit((1:num_cep).', jack_cep(:, col), 3), jack_cep_av_p)))
end
ind = find((unknown_csim > 0.9 & unknown_norm < 0.06 & unknown_dnorm < 0.01));
if length(ind) > 1
    total_val = (1 - unknown_csim(ind)) + unknown_norm(ind) + unknown_dnorm(ind);
    [mi, min_ind] = min(total_val);
    ind = ind(min_ind);
elseif length(ind) < 1
    total_val = (1 - unknown_csim) + unknown_norm + unknown_dnorm;
    [mi, ind] = min(total_val);
end
disp(ind)
% toc
