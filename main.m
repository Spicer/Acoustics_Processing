close all
clear; load('jack_data.mat')

sig = data(:,1);
t_vec = linspace(0, length(sig)/sample_rate-1/sample_rate, length(sig)).';

tracker = pll(t_vec, sig, sample_rate, 85);

[b_filt, a_filt] = butter(1, 150/sample_rate);
sig_filt = filtfilt(b_filt, a_filt, sig);
figure(); hold on
plot(t_vec, sig)
plot(t_vec, sig_filt)

corr_sig_filt = gainCorrection(sig_filt);

tracker_filt = pll(t_vec, corr_sig_filt, sample_rate, 85);

sine = sin(85*2*pi*t_vec+45*180/pi);
sine = sine + randn(size(sine))/9;
sine_track = pll(t_vec, sine, sample_rate, 85);
[freqs, periodic_sig] = hilbertPeriodic(sine_track, t_step);
[freq, fft_data] = posFFT(sine_track, sample_rate, 0);
figure(); plot(freq, fft_data)

h_freq_arr = hilbertFreqs(sine_track, t_step);
figure(); plot(h_freq_arr)
