function corrected_sig = gainCorrection(sig)

corrected_sig = sig * 1/mean(max(sig));

end