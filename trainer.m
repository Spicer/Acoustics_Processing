close all
clear

num_cep = 12;

tic
load('jack_data.mat')
jack_cep = zeros(num_cep, size(data, 2));
for col = 1:size(data, 2)
    jack_cep(:, col) = analyze(data(:, col), sample_rate, num_cep);
end
figure(); plot(jack_cep)
jack_cep_av = mean(jack_cep, 2);
% jack_cep_av_p = polyfit((1:num_cep).', jack_cep_av, 3);
jack_cep_corr = corrcoef(jack_cep);
% disp(KLDiv(jack_cep.',jack_cep_av.'))
jack_csim = zeros(size(data, 2), 1);
for col = 1:size(data, 2)
    jack_csim(col) = cosineSimilarity(jack_cep(:, col), jack_cep_av);
%     disp(min(corrcoef(jack_cep(:, col), jack_cep_av)))
%     disp(min(corrcoef(polyfit((1:num_cep).', jack_cep(:, col), 3), jack_cep_av_p)))
end
disp(min(jack_csim))
toc

load('matt_data.mat')
matt_cep = zeros(num_cep, size(data, 2));
for col = 1:size(data, 2)
    matt_cep(:, col) = analyze(data(:, col), sample_rate, num_cep);
end
figure(); plot(matt_cep)
matt_cep_corr = corrcoef(matt_cep);
% disp(KLDiv(matt_cep.',jack_cep_av.'))
matt_csim = zeros(size(data, 2), 1);
for col = 1:size(data, 2)
    matt_csim(col) = cosineSimilarity(matt_cep(:, col), jack_cep_av);
%     disp(min(corrcoef(matt_cep(:, col), jack_cep_av)))
%     disp(min(corrcoef(polyfit((1:num_cep).', jack_cep(:, col), 3), jack_cep_av_p)))
end
disp(max(matt_csim))
toc

load('adam_data.mat')
adam_cep = zeros(num_cep, size(data, 2));
for col = 1:size(data, 2)
    adam_cep(:, col) = analyze(data(:, col), sample_rate, num_cep);
end
figure(); plot(adam_cep)
adam_cep_corr = corrcoef(adam_cep);
% disp(KLDiv(adam_cep.',jack_cep_av.'))
adam_csim = zeros(size(data, 2), 1);
for col = 1:size(data, 2)
    adam_csim(col) = cosineSimilarity(adam_cep(:, col), jack_cep_av);
%     disp(min(corrcoef(adam_cep(:, col), jack_cep_av)))
%     disp(min(corrcoef(polyfit((1:num_cep).', jack_cep(:, col), 3), jack_cep_av_p)))
end
disp(max(adam_csim))
toc

disp(mean(jack_csim)), disp(mean(matt_csim)), disp(mean(adam_csim))
figure(); hold on; plot(jack_csim); plot(matt_csim); plot(adam_csim)

save('trained_data.mat')
