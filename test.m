close all
clear
load('jack_data.mat'); jack.data = data;
[jack.freqs, jack.auto] = autospectrum(jack.data, sample_rate);
jack.cont = constructHarmonicMatrix(jack.data, sample_rate, 0);
jack.freq_pd = fitdist(squeeze(jack.cont(1,1,:)), 'LogNormal');
load('matt_data.mat'); matt.data = data;
[matt.freqs, matt_auto] = autospectrum(matt.data, sample_rate);
matt.cont = constructHarmonicMatrix(matt.data, sample_rate, 0);
matt.freq_pd = fitdist(squeeze(matt.cont(1,1,:)), 'LogNormal');
% load('matt_sick_data.mat'); sick_data = data;
% [sick_freqs, sick_auto] = autospectrum(sick_data, sample_rate);
% sick_cont = constructHarmonicMatrix(sick_data, sample_rate, 0);
load('adam_data.mat'); adam.data = data;
[adam.freqs, adam.auto] = autospectrum(adam.data, sample_rate);
adam.cont = constructHarmonicMatrix(adam.data, sample_rate, 0);
adam.freq_pd = fitdist(squeeze(adam.cont(1,1,:)), 'LogNormal');

load('unknown_data1.mat'); unknown.data = data;
[unknown.freqs, unknown.auto] = autospectrum(unknown.data, sample_rate);
unknown.cont = constructHarmonicMatrix(unknown.data, sample_rate, 0);
unknown.freq_pd = fitdist(squeeze(unknown.cont(1,1,:)), 'LogNormal');

jack.cos_sim = zeros(size(jack.cont, 3)-1, 1);
for jj = 2:size(jack.cont, 3)
    jack.cos_sim(jj-1) = cosineSimilarity(jack.cont(:, 2, 1), jack.cont(:, 2, jj));
end

jack_matt_cos_sim = zeros(size(jack.cont, 3)-1, 1);
for jj = 2:size(matt.cont, 3)
    jack_matt_cos_sim(jj-1) = cosineSimilarity(jack.cont(:, 2, 1), matt.cont(:, 2, jj));
end

jack_adam_cos_sim = zeros(size(jack.cont, 3)-1, 1);
for jj = 2:size(adam.cont, 3)
    jack_adam_cos_sim(jj-1) = cosineSimilarity(jack.cont(:, 2, 1), adam.cont(:, 2, jj));
end

jack.kl_div = KLDiv(squeeze(jack.cont(:, 2, 2:end)).', jack.cont(:, 2, 1).');
jack.kl_pd = fitdist(jack.kl_div, 'Kernel', 'support', 'positive');
figure(); hist(jack.kl_div); hold on
x_values = 0:0.001:1;
y = pdf(jack.kl_pd,x_values);
plot(x_values,y,'LineWidth',2)
jack_matt_kl_div = KLDiv(squeeze(jack.cont(:, 2, :)).', squeeze(matt.cont(:, 2, 1:20)).');
jack_adam_kl_div = KLDiv(squeeze(jack.cont(:, 2, :)).', squeeze(adam.cont(:, 2, 1:20)).');

matt.kl_div = KLDiv(squeeze(matt.cont(:, 2, 2:end-10)).', matt.cont(:, 2, 1).');
matt.kl_pd = fitdist(matt.kl_div, 'Kernel', 'support', 'positive');
figure(); hist(matt.kl_div); hold on
x_values = 0:0.001:1;
y = pdf(matt.kl_pd,x_values);
plot(x_values,y,'LineWidth',2)
matt_adam_kl_div = KLDiv(squeeze(matt.cont(:, 2, :)).', squeeze(adam.cont(:, 2, :)).');

adam.kl_div = KLDiv(squeeze(adam.cont(:, 2, 2:end-10)).', adam.cont(:, 2, 1).');
adam.kl_pd = fitdist(adam.kl_div, 'Kernel', 'support', 'positive');
figure(); hist(adam.kl_div); hold on
x_values = 0:0.001:1;
y = pdf(adam.kl_pd,x_values);
plot(x_values,y,'LineWidth',2)

unknown.kl_div = KLDiv(squeeze(unknown.cont(:, 2, 1:end)).', ...
    mean(squeeze(jack.cont(:, 2, :)), 2).');
unknown.kl_pd = fitdist(unknown.kl_div, 'Kernel', 'support', 'positive');
figure(); hist(unknown.kl_div); hold on
x_values = 0:0.001:1;
y = pdf(unknown.kl_pd,x_values);
plot(x_values,y,'LineWidth',2)

peeps = ['jack'; 'matt'; 'sick'; 'adam'];
num_peeps = length(peeps);

% Convert to MFCCs very close to those genrated by feacalc -sr 22050 -nyq 8000
% -dith -hpf -opf htk -delta 0 -plp no -dom cep -com yes -frq mel -filt tri -win 32
% -step 16 -cep 20
% mult 3.3752
jack.mel.mm = zeros(12, 77, size(jack.data, 2));
for col = 1:size(jack.data, 2)
    [jack.mel.mm(:,:,col),aspc] = melfcc(jack.data(:,col)*2^15, ...
        sample_rate, 'maxfreq', 8000, 'numcep', 12, 'nbands', 22, ...
        'fbtype', 'fcmel', 'dcttype', 1, 'usecmp', 1, 'wintime', 0.032, ...
        'hoptime', 0.016, 'preemph', 0, 'dither', 1);
end
jack.mel.mm = mean(jack.mel.mm, 2);
jack.mel.mm = mean(squeeze(jack.mel.mm), 2);

unknown.mel.mm = zeros(12, 77, size(unknown.data, 2));
for col = 1:size(unknown.data, 2)
    [unknown.mel.mm(:,:,col),aspc] = melfcc(unknown.data(:,col)*2^15, ...
        sample_rate, 'maxfreq', 8000, 'numcep', 12, 'nbands', 22, ...
        'fbtype', 'fcmel', 'dcttype', 1, 'usecmp', 1, 'wintime', 0.032, ...
        'hoptime', 0.016, 'preemph', 0, 'dither', 1);
end
unknown.mel.mm = squeeze(mean(unknown.mel.mm, 2));

unknown.mel.kl_div = KLDiv(unknown.mel.mm.', jack.mel.mm.');

% .. then convert the cepstra back to audio (same options)
[im,ispc] = invmelfcc(mm, sr, 'maxfreq', 8000, 'numcep', 12, 'nbands', 22, ...
    'fbtype', 'fcmel', 'dcttype', 1, 'usecmp', 1, 'wintime', 0.032, ...
    'hoptime', 0.016, 'preemph', 0, 'dither', 1);

figure()
hold on
stem(jack_freqs, abs(mean(jack_auto, 2)))
stem(matt.freqs, abs(mean(matt_auto, 2)))
stem(sick_freqs, abs(mean(sick_auto, 2)))
stem(adam.freqs, abs(mean(adam.auto, 2)))
legend('jack', 'matt', 'sick', 'adam')
title('Averaged FFTs')

figure()
hold on
stem(jack_freqs, abs(jack_auto(:, 2)))
stem(matt.freqs, abs(matt_auto(:, 2)))
stem(sick_freqs, abs(sick_auto(:, 2)))
stem(adam.freqs, abs(adam.auto(:, 2)))
legend('jack', 'matt', 'sick', 'adam')
title('Single FFTs')

sig = data(:,1);
t_vec = linspace(0, length(sig)/sample_rate-1/sample_rate, length(sig)).';



tracker = pll(t_vec, sig, sample_rate, 85);

[b_filt, a_filt] = butter(1, 150/(sample_rate/2));
sig_filt = filtfilt(b_filt, a_filt, sig);
figure(); hold on
plot(t_vec, sig)
plot(t_vec, sig_filt)

corr_sig_filt = gainCorrection(sig_filt);

tracker_filt = pll(t_vec, corr_sig_filt, sample_rate, 85);

sine = sin(85*2*pi*t_vec+45*180/pi);
sine = sine + randn(size(sine))/9;
sine_track = pll(t_vec, sine, sample_rate, 85);
[freqs, periodic_sig] = hilbertPeriodic(sine_track, t_step);
[freq, fft_data] = posFFT(sine_track, sample_rate, 0);
figure(); plot(freq, fft_data)

h_freq_arr = hilbertFreqs(sine_track, t_step);
figure(); plot(h_freq_arr)
