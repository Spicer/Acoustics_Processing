function mat = getHarmonicContent(data, num_freqs, sample_rate)

% num_freqs = 5;
mat = zeros(num_freqs,2);

[freq, auto] = autospectrum(data, sample_rate);

cutoff = [50, 300]; %Hz
[b_filt, a_filt] = butter(1, cutoff/sample_rate);
filt_data = filtfilt(b_filt, a_filt, data);
[~, filt_auto] = autospectrum(filt_data, sample_rate);
max_ind = findFundFreq(freq, filt_auto);

% mat = [freq(max_indices), fft_data(max_indices)];

max_inds = (1:num_freqs).' * max_ind;
for freq_num = 1:num_freqs
    inds = (max_inds(freq_num)-1:max_inds(freq_num)+1).';
    mat(freq_num,:) = [freq(max_inds(freq_num)), mean(auto(inds))];
end

mat(:,2) = mat(:,2)/max(mat(:,2));

end