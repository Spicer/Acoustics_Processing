function max_ind = findFundFreq(freq, fft_data)

% [sorted_values,sort_index] = sort(fft_data(:),'descend');  %# Sort the values in
%                                                   %#   descending order
% max_index = sort_index(1:5);  %# Get a linear index into A of the 5 largest values

% [~, max_ind] = max(fft_data);

[~,locs,~,prom] = findpeaks(fft_data);
% des = prom>mean(prom)*2;
% locs = locs(des); prom=prom(des);
[~, sort_indices] = sort(prom,'descend');
locs = sort(locs(sort_indices(1:5)));
found = false;
ind = 1;

while ~found
    if freq(locs(ind)) < 300 && freq(locs(ind)) > 65
        found = true;
        max_ind = locs(ind);
    end
    ind = ind+1;
end
% max_indices = locs(sort_indices(1:5));

end