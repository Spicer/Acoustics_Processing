function mat = constructHarmonicMatrix(ensemble, sample_rate, num_freqs)

num_records = size(ensemble,2);
if num_freqs == 0
    num_freqs = 5;
end
mat = zeros(num_freqs,2,num_records);

for rec = 1:num_records
    mat(:,:,rec) = getHarmonicContent(ensemble(:,rec), num_freqs, sample_rate);
end

end